Nominatim
===========
This is the site for finding places using Latitude and Longitude


Repository:
===========
https://bitbucket.org/vasumohanlal/nominatim/


Install System Packages
=======================
python2.7


Getting source code
===================
git clone git@bitbucket.org:vasumohanlal/nominatim.git


Install following python packages
=================================
pip
virtualenv


Django-suit
^^^^^^^^^^^

See detailed `Django-suit documentation`_.

.. _`Django-suit documentation`: http://django-suit.readthedocs.io/en/develop/getting_started.html


Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^
* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.



Creating env & migrating DB
^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following are the commands to start the projects::

    $ source ~/<path_to_virtual_env>/bin/activate
    $ pip install -r requirements
    $ python manage.py makemigrations
    $ python manage.py migrate
    $ python manage.py runserver


