from models import Portal
from django.forms import ModelForm
from constants import *


class PortalForm(ModelForm):
    class Meta:
        model = Portal
        fields = ['name', 'contact_number', 'experience', 'resume']
