from __future__ import unicode_literals

from django.db import models
from .constants import *
import jsonfield
# Create your models here.

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/<filename>
    return "{}".format(filename)


class CoreMeta(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Portal(CoreMeta):
    name = models.CharField(max_length=255, blank=False, null=False, help_text="Enter your name in capital")
    contact_number = models.CharField(max_length=255, default=0, blank=False, unique=True, help_text="+919744XXXXXXX")
    experience = models.IntegerField(choices=EXP_TYPE, default=FRESHER, db_index=True,
                                     help_text="Enter your experience")
    resume = models.FileField(upload_to=user_directory_path, help_text="upload your resume .docx format only")
    status = models.IntegerField(choices=status, default=on_hold, db_index=True,
                                 help_text="applicant status")

    class Meta:
        db_table = 'resume_portal'

    def __unicode__(self):
        return "{}".format(self.name)


class Geolatlong(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False, help_text="Enter your name in capital")
    latitude = models.CharField(max_length=10, blank=False, null=False, help_text="latitude")
    longitude = models.CharField(max_length=10, blank=False, null=False, help_text="latitude")
    display_name = models.CharField(max_length=255, blank=False, null=False, help_text="Enter your name in capital")
    response_data = jsonfield.JSONField(default={})

    class Meta:
        db_table = 'geo_lat_long'

    def __unicode__(self):
        return "{}".format(self.display_name)
