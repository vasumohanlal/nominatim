from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .models import Portal
from .serializers import ApiSerializer, GpsSerializer


class GpsViewSet(viewsets.ModelViewSet):
    """
    A simple api for get lat ang long space
    """
    model = Portal
    serializer_class = GpsSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def list(self, *args, **kwargs):
        """
        Creates the Forum view once it has been reviewed
        """
        serializer = GpsSerializer()
        data = {"lat": -34.44076, "long": -58.70521}
        result_data = serializer.get(data)
        print result_data
        return Response({"status": "success", "message": result_data})


class PortalViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving resumes.
    """
    model = Portal
    serializer_class = ApiSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.model.objects.all()

    def list(self, request):
        """
        List all the Portal records which have been requested

        # Sample Output

            [
                {
                "name": "Mohanlal",
                "contact_number": "9597556148",
                "experience": 2,
                "resume": "/media/Feb2017Resume.docx",
                "is_accepted": false,
                "is_rejected": false
                },
                {
                "name": "Mohanlal",
                "contact_number": "234234234234",
                "experience": 5,
                "resume": "/media/Feb2017Resume_LyMpKHM.docx",
                "is_accepted": false,
                "is_rejected": false
                },
            ]

        """
        queryset = self.model.objects.all()
        serializer = ApiSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """
        Creates the Forum view once it has been reviewed
        """
        data = request.data.copy()
        serializer = ApiSerializer(data=data)
        is_valid = serializer.is_valid(raise_exception=True)
        if is_valid:
            serializer.save()
            return Response(serializer.data)
        else:
            return Response({"status": "error", "message": "Not in a valid format"})
