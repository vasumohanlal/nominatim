from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from views import PortalView
from blog_api import PortalViewSet, GpsViewSet


portal_list_create = PortalViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

gps_list = GpsViewSet.as_view({
    'get': 'list'
})

urlpatterns = [
    url(r'^portal/$', portal_list_create, name='List Messages'),
    url(r'^post/$', PortalView.as_view(), name='Resume Portal'),
    url(r'^nominatim/$', gps_list, name='Nominatim')
]

urlpatterns = format_suffix_patterns(urlpatterns)
