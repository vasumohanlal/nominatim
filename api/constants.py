FRESHER = 1
ASSOCIATE = 2
MIDLEVEL = 3
SENIOR = 4
MANAGER = 5

EXP_TYPE = (
    (FRESHER, 'FRESHER'),
    (ASSOCIATE, 'ASSOCIATE'),
    (MIDLEVEL, 'MIDLEVEL'),
    (SENIOR, 'SENIOR'),
    (MANAGER, 'MANAGER'),
)

Accepted = 1
Rejected = 2
on_hold = 3

status = (
    (Accepted, 'Accepted'),
    (Rejected, 'Rejected'),
    (on_hold, 'on_hold'),
)
