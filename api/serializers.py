from rest_framework import serializers
from api.models import Portal, Geolatlong
from .constants import *
import logging
import requests
import json

logger = logging.getLogger(__name__)


class ApiSerializer(serializers.Serializer):
    """
    Validates the request from the json and response --
    name
    contact_number
    experience
    resume
    is_accepted
    is_rejected
    """
    name = serializers.CharField(max_length=255, required=True, help_text="Enter your name in capital")
    contact_number = serializers.CharField(max_length=255, default=0, required=False, help_text="+919744XXXXXXX")
    experience = serializers.ChoiceField(choices=EXP_TYPE, default=FRESHER, help_text="Enter your experience")
    resume = serializers.FileField(max_length=None, allow_empty_file=False, use_url=True,
                                   help_text="upload your resume .docx format only")
    status = serializers.ChoiceField(choices=status, default=on_hold, help_text="applicant status")

    def create(self, validated_data):
        """
        Create and return a new `Resume record` instance, given the validated data.
        """
        new_obj = Portal()
        new_obj.name = self.initial_data.get('name')
        new_obj.contact_number = self.initial_data.get('contact_number')
        new_obj.experience = self.initial_data.get('experience')
        new_obj.resume = self.initial_data.get('resume')
        new_obj.status = self.initial_data.get('status', 3)
        new_obj.save()
        return new_obj


class GpsSerializer(serializers.Serializer):
    """
    validates the request from the json
    """
    def get(self, data):
        try:
            url = "https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat={}&lon={}".format(data["lat"],
                                                                                                   data["long"])
            r = requests.get(url)
            if r.status_code == 200:
                response_data = json.loads(r.text)
                new_obj = Geolatlong()
                new_obj.name = response_data["name"]
                new_obj.latitude = response_data["lat"]
                new_obj.longitude = response_data["lon"]
                new_obj.display_name = response_data["display_name"]
                new_obj.response_data = response_data
                new_obj.save()
                return "Data saved successfully"
            else:
                return {"Exception": "Doesn't find any matching lat or long to fetch data"}
        except Exception as e:
            return e.message
